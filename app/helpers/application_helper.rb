module ApplicationHelper
  SECRET_KEY = '816a311222c5aca73add3e11f9b17d6231cd4058'

  # Render error message
  def error_messages_for(object)
    # Check if object is an instance of ActiveRecord or ActiveModel.
    if object.instance_of?(ActiveRecord::Base) || object.class.ancestors.include?(ActiveModel::Model)
      render(partial: 'shared/error_messages', locals: { object: object } )
    else
      render(partial: 'shared/flash_error_message', locals: { object: object } )
    end
  end

  def captcha_for(object)
    render(partial: 'shared/captcha', locals: { object: object } )
  end

  def file_field_for(form, field, style)
    render(partial: 'shared/form_file_field', locals: { form: form, field: field, style: style } )
  end

  def encrypt(string)
    ActiveSupport::MessageEncryptor.new(SECRET_KEY).encrypt_and_sign(string)
  end

  def decrypt(string)
    ActiveSupport::MessageEncryptor.new(SECRET_KEY).decrypt_and_verify(string)
  end

  def current_url
    "#{request.protocol}#{request.host_with_port}#{request.fullpath}"
  end

  def avatar_url(user, size=48)
    default_url = "#{root_url}images/elements/avatar.png"
    gravatar_id = Digest::MD5.hexdigest(user.email.downcase)
    "http://gravatar.com/avatar/#{gravatar_id}.png?s=#{size.to_i}&d=#{CGI.escape(default_url)}"
  end

  def controller?(*controller)
    controller.include?(params[:controller])
  end

  def action?(*action)
    action.include?(params[:action])
  end

  def sortable(column, title = nil)
    title ||= column.titleize
    css_class = column == sort_column ? "current #{sort_direction}" : nil
    direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"
    link_to title, {sort: column, direction: direction}, { class: css_class }
  end

  def has_product_access?(entity)
    if current_user.is_admin?
      true
    end
    # Call Flow API or check cached scopes to see if user has access to product.
    false
  end
end
