class BaseModel::Entry < ActiveRecord::Base
  self.table_name = 'entry'
  self.per_page = 20

  belongs_to :country
  belongs_to :user

  as_enum :status_type,             { error: 0, disabled: 1, enabled: 2, pinned: 4 }, source: 'status'
  as_enum :main_display_mode_type,  { hide: 0, small: 1, medium: 2, large: 4 }, source: 'main_display_mode'

  scope :disabled,          -> { where(status: self.status_types[:disabled] ) }
  scope :enabled,           -> { where(status: self.status_types[:enabled] ) }
  scope :pinned,            -> { where(status: self.status_types[:pinned] ) }
  scope :public_accessible, -> { where(status: [ self.status_types[:enabled], self.status_types[:pinned] ] ) }
  scope :display_available, -> { where(main_display_mode: [ self.main_display_mode_types[:small], self.main_display_mode_types[:medium], self.main_display_mode_types[:large] ]) }
  scope :language,          ->(country_code) { where('country.code = ?', country_code).joins(:country) }
  scope :search,            ->(keywords) { where('entry.title LIKE :keywords OR entry.short_text LIKE :keywords', keywords: "%#{keywords}%") }

  has_attached_file :image, styles: {
      large:    [ '1200x300#', :jpg, ],
      medium:   [ '580x296#', :jpg, ],
      small:    [ '285x194#', :jpg, ],
      thumb:    [ '200x120>', :jpg, ]
  },
  convert_options: {
      large:    '-resize 1200x300^ -gravity center -crop 1200x300+0+0 -quality 80',
      medium:   '-quality 80',
      small:    '-quality 80'
  }

  has_attached_file :banner, styles: {
      regular:  [ '1200x150#', :jpg ],
      thumb:    [ '200x120>', :jpg ]
  },
  convert_options: {
      regular: '-resize 1200x150^ -gravity center -crop 1200x150+0+0 -quality 80'
  }

  validates :text,              presence:     true,
                                length:       100..100000
  validates :short_text,        presence:     true,
                                length:       50..10000
  validates :code,              allow_blank:  true,
                                length:       100..10000
  validates :title,             presence:     true,
                                length:       5..128
  validates :status,            presence:     true
  validates :main_display_mode, presence:     true
  validates :hits,              numericality: { only_integer: true }
  validates :country,           presence:     true,
                                existence:    true
  validates :user,              presence:     true,
                                existence:    true

  validates :image,             attachment_size: 0..5.megabytes,
                                attachment_content_type: { content_type: ['image/jpeg', 'image/png'], message: 'form.error.file_type' }
  validates :banner,            attachment_size: 0..5.megabytes,
                                attachment_content_type: { content_type: ['image/jpeg', 'image/png'], message: 'form.error.file_type' }

  after_initialize :init

  def init
    if self.new_record?
      self.main_display_mode = BaseModel::Entry.main_display_mode_types.small
      self.hits = 0
      self.status ||= BaseModel::Entry.status_types.disabled
    end
  end
end
