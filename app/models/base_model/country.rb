class BaseModel::Country < ActiveRecord::Base
  self.table_name = 'country'

  has_many :entries

  as_enum :payment_provider_type, { dot_pay: 'DotPay' }, source: 'payment_provider'

  validates :name,              presence:     true,
                                length:       4..32
  validates :currency_code,     presence:     true,
                                length:       { is: 3 }
  validates :code,              presence:     true,
                                length:       { is: 2 }
  validates :currency_symbol,   presence:     true,
                                length:       1..32
  validates :payment_provider,  presence:     true
  validates :tax,               presence:     true,
                                numericality: { greater_than_or_equal_to: 0.00, less_than_or_equal_to: 100.00 }
  validates :language,          presence:     true,
                                length:       { is: 2 }

  after_initialize :init

  def init
    self.tax = 0
  end
end
