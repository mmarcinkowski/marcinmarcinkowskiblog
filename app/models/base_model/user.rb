class BaseModel::User < ActiveRecord::Base
  AuthenticationError = Class.new(StandardError)
  AuthorizationError = Class.new(StandardError)

  self.table_name = 'user'

  authenticates_with_sorcery!
  simple_roles

  attr_accessor :password, :password_confirmation, :terms_of_service, :captcha

  has_many :entries

  scope :editors,       -> { where( role: :editor) }
  scope :admins,        -> { where( role: :admin) }
  scope :baned,         -> { where( status_type: :baned ) }
  scope :disabled,      -> { where( status_type: :disabled ) }
  scope :enabled,       -> { where( status_type: :enabled ) }
  scope :males,         -> { where( gender_type: :male ) }
  scope :females,       -> { where( gender_type: :female ) }
  scope :country_code,  ->(code) { where(country_id: code ) }

  as_enum :gender_type,  { male: 'M', female: 'F' }, source: 'gender'
  as_enum :status_type,  { removed: 0, baned: 1, disabled: 2, enabled: 3 }, source: 'status'

  validates :provider,              length:       1..100,
                                    allow_blank:  true
  validates :provider_id,           length:       1..100,
                                    allow_blank:  true
  validates :first_name,            length:       2..30,
                                    allow_blank:  true
  validates :last_name,             length:       2..30,
                                    allow_blank:  true
  validates :website,               url:          true,
                                    length:       10..100,
                                    allow_blank:  true
  validates :phone_number,          length:       4..20,
                                    allow_blank:  true
  validates :email,                 presence:     true,
                                    email:        true,
                                    length:       { maximum: 80 },
                                    uniqueness:   { case_sensitive: false }
  validates :password,              presence:     true,
                                    length:       { minimum: 6 },
                                    on:           :create
  validates :role,                  inclusion:    { in: SimpleRoles.config.valid_roles }
  validates :address,               length:       2..100,
                                    allow_blank:  true
  validates :location,              length:       2..100,
                                    allow_blank:  true
  validates :zip_code,              length:       2..10,
                                    allow_blank:  true
  validates :country_id,            inclusion:    { in: Country.all.to_a.map { |country| country[1] } },
                                    allow_blank:  true
  validates :status,                inclusion:    { in: status_types.values_at(:removed, :baned, :disabled, :enabled) }
  validates :gender,                presence:     true
  validates :birth_date,            presence:     true,
                                    date:         {
                                        after: Proc.new { Time.now - 100.years },
                                        before: Proc.new { Time.now - 6.year }
                                    }

  # Public validators
  validates :password,              confirmation: true,
                                    on:           :create,
                                    if:           :user_registration?
  validates :password_confirmation, presence:     true,
                                    on:           :create,
                                    if:           :user_registration?
  validates :terms_of_service,      acceptance:   '1',
                                    on:           :create,
                                    if:           :user_registration?

  before_create :generate_identification_token
  after_initialize :init

  def user_registration?
    # TODO method to check if action is registration to make extra validation work
    false
  end

  def new_email=(email)
    encryptor = ActiveSupport::MessageEncryptor.new(Digest::SHA1.hexdigest(self.salt))
    self.encrypted_new_email = encryptor.encrypt_and_sign(email)
  end

  def new_email
    encryptor = ActiveSupport::MessageEncryptor.new(Digest::SHA1.hexdigest(self.salt))
    encryptor.decrypt_and_verify(self.encrypted_new_email)
  end

  # Callback "before_create"
  # Set the default attributes.
  def generate_identification_token
    self.identification_token = Digest::MD5.hexdigest(Time.now.to_s+'23hgu23jv24')
  end

  def generate_removal_token
    self.removal_token = Digest::MD5.hexdigest(Time.now.to_s+'1d234rds2ji')
  end

  def deliver_removal_instructions
    self.generate_removal_token
    self.save && UserMailer.account_removal_email(self).deliver
  end

  def remove
    self.status = BaseModel::User.status_types[:removed]
    self.email = nil
    self.removal_token = nil
    self.save!(validate: false)
  end

  def reset_password(password)
    self.password_confirmation = password
    self.change_password!(password)
  end

  def reset_email(email)
    self.email = email
    self.encrypted_new_email = nil
    self.save!
  end

  def able_to_logged_in?
    [:enabled].include? self.status_type
  end

  def full_name
    if first_name.present? || last_name.present?
      (first_name.to_s+' '+last_name.to_s).strip
    end
  end

  def full_name_or_email
    if first_name.present? || last_name.present?
      (first_name.to_s+' '+last_name.to_s).strip
    else
      email
    end
  end

  def full_name_with_email
    (first_name+' '+last_name+' ('+email+')').strip
  end

  def website_name
    if website
      URI.parse(website).host
    end
  end

  def self.load_from_encrypted_new_email(encrypted_email)
    return nil if encrypted_email.blank?
    user = find_by_encrypted_new_email(encrypted_email)
    return user.blank? ? nil : user
  end

  def self.load_from_removal_token(token)
    return nil if token.blank?
    user = find_by_removal_token(token)
    return user.blank? ? nil : user
  end

  def self.load_from_identification_token(token)
    return nil if token.blank?
    user = find_by_identification_token(token)
    return user.blank? ? nil : user
  end

  def admin_update_attributes(attributes)
    if self.valid?
      if (attributes.include?(:password))
        self.reset_password(attributes[:password])
        attributes.delete(:password)
      end
      self.update_attributes(attributes)
    end
  end

  # Update user settings
  def update_settings!(params)
    self.update_attributes({
      first_name: params[:first_name],
      last_name: params[:last_name],
      website: params[:website],
      phone_number: params[:phone_number],
      address: params[:address],
      zip_code: params[:zip_code],
      location: params[:location],
      country_id: params[:country_id],
    })
  end

  # Activate user account
  def activate!
    self.generate_identification_token
    self.status = BaseModel::User.status_types[:enabled]
    super
  end

  private

  # Callback "before_validate"
  # Set the default attributes.
  def init
    if self.new_record?
      self.status ||= BaseModel::User.status_types[:disabled]
      self.role ||= :user
    end
  end
end
