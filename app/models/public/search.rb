class Public::Search
  include ActiveModel::Model

  attr_accessor :keywords

  validates :keywords,              presence:       true,
                                    length:         3..50

  def find_courses(page=1, is_admin = false)
    if is_admin
      BaseModel::Course.search(keywords).language(I18n.locale).paginate(page: page, per_page: 6).order('status DESC, id DESC')
    else
      BaseModel::Course.search(keywords).language(I18n.locale).public_accessible().paginate(page: page, per_page: 6).order('status DESC, id DESC')
    end
  end

  def find_entries(page=1, is_admin = false)
    if is_admin
      BaseModel::Entry.search(keywords).language(I18n.locale).display_available().paginate(page: page, per_page: 6).order('status DESC, id DESC')
    else
      BaseModel::Entry.search(keywords).language(I18n.locale).public_accessible().display_available().paginate(page: page, per_page: 6).order('status DESC, id DESC')
    end
  end
end