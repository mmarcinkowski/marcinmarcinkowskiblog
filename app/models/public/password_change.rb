class Public::PasswordChange
  include ActiveModel::Model

  attr_accessor :user, :old_password, :new_password, :new_password_confirmation

  validates :user,                        presence:       true
  validates :new_password,                presence:       true,
                                          length:         { minimum: 6 },
                                          confirmation:   true
  validates :new_password_confirmation,   presence:       true
  validates :old_password,                presence:       true,
                                          authentication: { user: :user }

  def change
    self.valid? && self.user.reset_password(self.new_password)
  end
end