class Public::PasswordRecovery
  include ActiveModel::Model

  attr_accessor :user, :email

  validates :email,                 presence:   true,
                                    email: true,
                                    value_existence: { model: BaseModel::User, fields: [ :email ], message: I18n.t('errors.messages.email_value_existence')}

  def deliver_password_recovery_instructions!
    if self.valid?
      self.user = BaseModel::User.find_by_email(self.email)

      if self.user.present?
        # May return false because of time limitations for email sending
        self.user.deliver_reset_password_instructions!
      end

      true  # Return true anyway if valid
    end
  end

end