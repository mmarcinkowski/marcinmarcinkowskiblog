class Public::Session
  include ActiveModel::Model

  attr_accessor :email, :password, :remember_me

  validates :password,                    presence:     true,
                                          length:       { minimum: 6 }
  validates :email,                       presence:     true,
                                          email:        true
end