class Public::PasswordReset
  include ActiveModel::Model

  attr_accessor :user, :password, :password_confirmation

  validates :user,                  presence:       true
  validates :password,              presence:       true,
                                    length:         { minimum: 6 },
                                    confirmation:   true
  validates :password_confirmation, presence:       true

  def reset
    self.valid? && self.user.reset_password(self.password)
  end
end