class AuthenticationValidator < ActiveModel::EachValidator

  def validate_each(record, attribute, value)

    if options[:user].present?
      unless Public::User.authenticate(record.send(options[:user]).email, value)
        record.errors[attribute] << (options[:message] ||  I18n.t('errors.messages.wrong_password'))
      end
    else
      record.errors[attribute] << (options[:message] ||  I18n.t('errors.messages.user_not_found'))
    end
  end
end