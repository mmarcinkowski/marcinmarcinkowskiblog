class UrlValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    unless value =~ URI.regexp
      record.errors[attribute] << (options[:message] || I18n.t('errors.messages.not_valid_url'))
    end
  end
end