class Public::PasswordChangesController < Public::PublicController

  layout 'dashboard'

  def edit
    @password_change = Public::PasswordChange.new
    render 'edit'
  end

  def update
    @password_change = Public::PasswordChange.new(password_change_params.merge(user: current_user))

    respond_to do |format|
      if @password_change.change
        format.html {
          flash[:success] = t 'notice.success.password_changed'
          redirect_to controller: 'public/dashboards', action: 'show'
        }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @password_change.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def password_change_params
    params.require(:public_password_change).permit(:old_password, :new_password, :new_password_confirmation)
  end

end