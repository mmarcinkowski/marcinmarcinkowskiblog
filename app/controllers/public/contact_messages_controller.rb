class Public::ContactMessagesController < Public::PublicController
  skip_before_filter :require_login, only: [:new, :create]

  # Show the form for contact message
  def new
    @contact_message = Public::ContactMessage.new
    render 'new'
  end

  # Send an contact message
  def create
    @contact_message = Public::ContactMessage.new(contact_message_params)

    respond_to do |format|
      if verify_recaptcha(model: @contact_message, attribute: :captcha, message: t('form.error.captcha')) && @contact_message.send_contact_message
        format.html do
          flash[:success] = t 'notice.success.contact_message_sent'
          redirect_to root_path
        end
        format.json { render json: @contact_message, status: :created }
      else
        format.html { render action: 'new' }
        format.json { render json: @contact_message.errors, status: :unprocessable_entity }
      end
    end
  end


  private

  def contact_message_params
    params.require(:public_contact_message).permit(:email, :name, :subject, :body)
  end

end