class Public::PagesController < Public::PublicController
  skip_before_filter :require_login
  before_action :set_pages

  attr_accessor :pages

  def set_pages
    @pages = ['home']
  end

  def show
    page = params[:id]
    @pages.include?(page) || not_found
    # Call the method or render view by name
    self.respond_to?(page) ? self.send(page) : render(page)
  end

  def home
    if current_user && current_user.is_admin?
      @entries = BaseModel::Entry.language(I18n.locale).display_available().paginate(page: params[:page]).order('status DESC, id DESC')
    else
      @entries = BaseModel::Entry.language(I18n.locale).public_accessible().display_available().paginate(page: params[:page]).order('status DESC, id DESC')
    end

    respond_to do |format|
      format.html { render 'home' }
      format.json { render json: @entries }
    end
  end
end
