class Public::PasswordResetsController < Public::PublicController
  skip_before_filter :require_login

  # Password recovery form.
  def new
    return redirect_logged_in if logged_in?

    @password_recovery = Public::PasswordRecovery.new
    render 'new'
  end

  # Request password reset.
  def create
    return redirect_logged_in if logged_in?

    @password_recovery = Public::PasswordRecovery.new(password_recovery_params)

    respond_to do |format|
      if @password_recovery.deliver_password_recovery_instructions!
        format.html { redirect_to action: 'created' }
        format.json { head :no_content }
      else
        format.html { render action: 'new' }
        format.json { render json: @password_recovery.errors, status: :unprocessable_entity }
      end
    end
  end

  # Information that our system sent a message with instructions about password reset.
  def created
    return redirect_logged_in if logged_in?

    render 'created'
  end


  # This is the reset password form.
  def edit
    @user = BaseModel::User.load_from_reset_password_token(params[:id] || params[:token]) || not_authorized
    @password_reset = Public::PasswordReset.new(user: @user)

    render 'edit'
  end

  # This action fires when the user has sent the reset password form.
  def update
    @user = BaseModel::User.load_from_reset_password_token(params[:id] || params[:token]) || not_authorized
    @password_reset = Public::PasswordReset.new(password_reset_params.merge(user: @user))

    respond_to do |format|
      if @password_reset.reset
        format.html do
          flash[:success] = t 'notice.success.password_reset'
          redirect_to new_sessions_path
        end
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @password_reset.errors, status: :unprocessable_entity }
      end
    end
  end

  private


  def password_reset_params
    params.require(:public_password_reset).permit(:password, :password_confirmation)
  end

  def password_recovery_params
    params.require(:public_password_recovery).permit(:email)
  end
end