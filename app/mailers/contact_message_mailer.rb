class ContactMessageMailer < ActionMailer::Base
  default from: 'no-reply@practicalknowledge.com'
  default to: APP_CONFIG['contact']['email']
  layout 'email'

  def contact_message_email(contact_message)
    @name = contact_message.name
    @body = contact_message.body
    mail(reply_to: contact_message.email, subject: contact_message.subject)
  end
end
