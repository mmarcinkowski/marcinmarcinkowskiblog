class UserMailer < ActionMailer::Base
  default from: 'no-reply@practicalknowledge.com'
  layout 'email'

  def activation_needed_email(user)
    @user = user
    mail(to: user.email, subject: t('mailer.subject.registration_confirmation'))
  end

  def activation_success_email(user)
    @user = user
    mail(to: user.email, subject: t('mailer.subject.account_activated'))
  end

  def account_removal_email(user)
    @user = user
    mail(to: user.email, subject: t('mailer.subject.removal_confirmation'))
  end

  def reset_password_email(user)
    @user = user
    mail(to: user.email, subject: t('mailer.subject.password_reset'))
  end

  def change_email_email(user)
    @user = user
    mail(to: user.new_email, subject: t('mailer.subject.email_change')) if user.new_email.present?
  end
end
