SimpleRoles.configure do
  valid_roles :user, :admin, :editor
  strategy :one # Default is :one
end