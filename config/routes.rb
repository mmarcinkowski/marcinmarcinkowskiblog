Rails.application.routes.draw do

  # Root Homepage
  root                  to: 'public/pages#home'
  get '/:locale',       to: 'public/pages#home'

  # Local routes
  scope '/:locale' do

    resources :errors, only: [:show]

    namespace :admin do
      resources :users, :countries, :entries, :categories

      resources :courses do
        resources :lessons
      end

      # Attachments resource
      match '/attachments/:model/:id/:attachment/:return_url/(.:format)' => 'attachments#destroy', as: :attachment, via: [:delete, :get, :post]
    end

    scope module: 'public' do

      # Search resource aliases
      get     'search',                       to: 'searches#show'

      # Pages resource aliases
      get     '',                             to: 'pages#show', id: 'home'

      # Sessions resource aliases
      get     'login',                        to: 'sessions#new'
      post    'login',                        to: 'sessions#create'
      get     'logout',                       to: 'sessions#destroy'

      # User resource aliases
      get     'user/register',                to: 'users#new'
      post    'user/register',                to: 'users#create'
      get     'user/registered/:token',       to: 'users#created'
      get     'user/activate/:token',         to: 'users#activate'
      get     'user/activated/:token',        to: 'users#activated'
      get     'user/edit',                    to: 'users#edit'
      patch   'user/edit',                    to: 'users#update'
      get     'user/delete',                  to: 'users#delete'
      patch   'user/delete',                  to: 'users#activate_removal'
      get     'user/remove/:token',           to: 'users#destroy'
      get     'user/removed/:token',          to: 'users#removed'

      # Contact messages
      get     'contact',                      to: 'contact_messages#new'
      post    'contact',                      to: 'contact_messages#create'

      # Email change aliases
      get     'user/email-change',            to: 'email_changes#new'
      post    'user/email-change',            to: 'email_changes#create'
      get     'user/email-change/:token',     to: 'email_changes#update'

      # Password change aliases
      get     'user/password-change',         to: 'password_changes#edit'
      put     'user/password-change',         to: 'password_changes#update'

      # Password reset aliases
      get     'user/password-reset',          to: 'password_resets#new'
      post    'user/password-reset',          to: 'password_resets#create'
      get     'user/password-reset/:token',   to: 'password_resets#edit'
      put     'user/password-reset/:token',   to: 'password_resets#update'

      # Users resource
      resources :users do
        member do
          get 'created'             # Information page that activation email was sent.
          get 'activate'            # Activate user account.
          get 'activated'           # Welcome page for activated user.
          get 'delete'              # Form to remove account.
          patch 'activate_removal'  # Sent an email message with confirmation link for removal process.
          get 'removed'             # Message that account had been removed.
        end

        # Password Change resource
        resource :password_change,  only: [:edit, :update]

        # Email Change resource
        resource :email_change,  only: [:new, :create, :update]
      end

      # Password reset resource
      resources :password_resets, only: [:new, :create, :edit, :update] do
        collection do
          get 'created'
        end
      end

      # Pages resource
      resources :pages, only: [:show]

      # Contact messages resource
      resources :contact_messages,     only: [:new, :create]

      # Entries resource
      resources :entries,     only: [:show]

      # Categories resource
      # resources :categories,  only: [ :show ]

      # Search resource
      resource :searches,    only: [:show]

      # Sessions resource
      resource  :sessions,    only: [:new, :create, :destroy]

      # Dashboard resource
      resource  :dashboard,   only: [:show]

    end

  end

  # mount Ckeditor::Engine => '/ckeditor'
end
