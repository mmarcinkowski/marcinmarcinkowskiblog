class AddImageColumns < ActiveRecord::Migration
  def up
    add_attachment :entry, :image
    add_attachment :entry, :banner
  end

  def down
    remove_attachment :entry, :image
    remove_attachment :entry, :banner
  end
end
