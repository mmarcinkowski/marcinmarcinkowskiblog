# -*- coding: utf-8 -*-
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


# Load Users
user = BaseModel::User.new({email:'inf.marcinkowski@gmail.com', password:'testtest', gender:BaseModel::User.gender_types[:male], birth_date:'1988-11-10', first_name:'Marcin', last_name:'Marcinkowski'})
user.ip = '127.0.0.1'
user.role = :admin
user.country_id = 'PL'
user.status = BaseModel::User.status_types[:enabled]
user.save
user.activate!

user = BaseModel::User.new({email:'m2ka@o2.pl', password:'testtest', gender:BaseModel::User.gender_types[:male], birth_date:'1988-11-10', first_name:'Marcin', last_name:'Marcinkowski'})
user.ip = '127.0.0.1'
user.role = :editor
user.country_id = 'PL'
user.status = BaseModel::User.status_types[:enabled]
user.save
user.activate!

user = BaseModel::User.new({email:'test@test.com', password:'testtest', gender:BaseModel::User.gender_types[:male], birth_date:'1979-01-09', first_name:'Michał', last_name:'Testowy'})
user.ip = '127.0.0.1'
user.role = :editor
user.country_id = 'PL'
user.status = BaseModel::User.status_types[:enabled]
user.activate!

(1..10).each do |index|
  user = BaseModel::User.new({email:'exemple'+(index.to_s)+'@gmail.com', password:'testtest', gender:BaseModel::User.gender_types[:male], birth_date:'1990-11-10', first_name:'NameExample', last_name:'LastNameExample'})
  user.ip = '127.0.0.1'
  user.role = :user
  user.country_id = 'PL'
  user.status = BaseModel::User.status_types[:enabled]
  user.save
  user.activate!
end

# Load Countries
country1 = BaseModel::Country.new({name:'Polska', code:'pl', currency_code:'pln', currency_symbol:'zł', tax:19, language: 'pl', payment_provider:BaseModel::Country.payment_provider_types[:dot_pay] })
country1.save
country2 = BaseModel::Country.new({name:'Deutschland ', code:'de', currency_code:'eur', currency_symbol:'€', tax:25, language: 'en', payment_provider:BaseModel::Country.payment_provider_types[:dot_pay] })
country2.save
country3 = BaseModel::Country.new({name:'USA', code:'us', currency_code:'usd', currency_symbol:'$', tax:10, language: 'en', payment_provider:BaseModel::Country.payment_provider_types[:dot_pay] })
country3.save

# Load Categories
category1 = BaseModel::Category.new({name:'PHP'})
category1.save
category2 = BaseModel::Category.new({name:'Ruby'})
category2.save
category3 = BaseModel::Category.new({name:'Java'})
category3.save
category4 = BaseModel::Category.new({name:'Android'})
category4.save

# Load Entities
(1..30).each do |index|
  entry = BaseModel::Entry.new
  entry.title = 'Title Entry Example '+index.to_s
  entry.main_display_mode = BaseModel::Entry.main_display_mode_types([:small, :medium].sample)
  entry.status = BaseModel::Entry.status_types(:enabled)
  entry.user_id = user.id
  entry.country_id = country1.id
  entry.text = '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque imperdiet congue elit in condimentum. Pellentesque egestas dapibus turpis, in facilisis dolor accumsan vitae. Morbi diam eros, sodales at fermentum id, tempor quis ligula. Curabitur est ligula, pulvinar eget dapibus ut, vehicula ac dolor. Phasellus eu nulla mi, vel mattis elit. Etiam sit amet est turpis. Nullam hendrerit accumsan turpis. Sed viverra mi ac lorem venenatis ultricies. Proin tempor lorem ultrices neque mattis non porttitor odio tempor. Donec pretium rutrum ligula fringilla pretium. Mauris non eros a purus vulputate faucibus. Nunc lacinia commodo luctus. Nulla imperdiet odio a urna elementum iaculis. Vestibulum est quam, tincidunt et pulvinar et, tincidunt vel dolor. Phasellus sem diam, eleifend a fermentum nec, hendrerit at ligula. Vestibulum ac augue nulla, eu consequat metus.</p><p>Aliquam cursus vestibulum diam, ut gravida lectus dapibus et. Fusce auctor convallis lectus, ac volutpat nulla vehicula ut. Curabitur in velit vitae risus sodales tincidunt ac quis sapien. Curabitur condimentum velit sit amet leo aliquet non adipiscing tellus fermentum. Proin ligula mi, sodales ut gravida sit amet, molestie eu nulla. Aliquam orci turpis, pulvinar tincidunt consequat in, commodo tempus diam. Maecenas turpis sem, mattis eu dignissim auctor, imperdiet nec velit. Nam egestas facilisis leo quis elementum.</p><p>Donec aliquet elit eget enim dapibus id sodales nulla sollicitudin. Aliquam lacus nunc, condimentum a tincidunt a, dignissim sed metus. Quisque cursus eleifend ipsum, vel tincidunt odio porta ac. Nullam commodo ullamcorper nisl, et luctus odio fermentum pellentesque. Maecenas a felis augue, id porttitor diam. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla eleifend quam ac eros tempor malesuada.</p>'
  entry.short_text = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque imperdiet congue elit in condimentum. Pellentesque egestas dapibus turpis, in facilisis dolor accumsan vitae. Morbi diam eros, sodales at fermentum id, tempor quis ligula. Curabitur est ligula, pulvinar eget dapibus ut, vehicula ac dolor. Phasellus eu nulla mi, vel mattis elit. Etiam sit amet est turpis. Nullam hendrerit accumsan turpis. Sed viverra mi ac lorem venenatis ultricies.'
  entry.save
end

# Load Courses
(1..20).each do |index|
  course = BaseModel::Course.new
  course.user_id = user.id
  course.country_id = country1.id
  course.category_id = [category1.id, category2.id, category3.id, category4.id].sample
  course.title = 'Title Course Example '+index.to_s
  course.text = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque imperdiet congue elit in condimentum. Pellentesque egestas dapibus turpis, in facilisis dolor accumsan vitae. Morbi diam eros, sodales at fermentum id, tempor quis ligula. Curabitur est ligula, pulvinar eget dapibus ut, vehicula ac dolor.'
  course.short_text = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque imperdiet congue elit in condimentum. Pellentesque egestas dapibus turpis, in facilisis dolor accumsan vitae. Morbi diam eros, sodales at fermentum id, tempor quis ligula. Curabitur est ligula, pulvinar eget dapibus ut, vehicula ac dolor.'
  course.price = 12.77
  course.duration = 10000
  course.status = BaseModel::Course.status_types([:free, :regular, :pinned].sample)
  course.level = BaseModel::Course.level_types([:beginner, :intermediate, :advanced].sample)
  course.subject = BaseModel::Course.subject_types([:text, :audio, :video].sample)
  course.save
end

